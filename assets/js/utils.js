console.log('UTILS::: utils loaded');
function isPepperFlash() {
	if (navigator.plugins) {
		for (var i=0, count = navigator.plugins.length; i < count; i++) {
			var filename = navigator.plugins[i].filename;
			if (filename === 'pepflashplayer.dll' || filename === 'PepperFlashPlayer.plugin') {
				return true;
			}
		}
	}
	return false;
}

function showChromeWarning() {
	var msg = "Please use headphones!\n\n";
	msg += "Streaming is not optimized for the Chrome browser. Connecting your webcam through Chrome can cause an echo when using your microphone.\n\n";
	msg += "It is recommended to use Firefox, Internet Explorer or Safari. \n";
	msg += "To continue using Chrome please use headphones.";
	alert(msg);
}

/*
  Prototype Event Dispatcher
*/
function EventDispatcher() {console.log('EVENTDISPATCHER::: init');}
EventDispatcher.prototype.events = {};
EventDispatcher.prototype.sayHello = function() {console.log('EVENTDISPATCHER::: HELLO');};
EventDispatcher.prototype.addEventListener = function (key, func) {
    if (!this.events.hasOwnProperty(key)) {
        this.events[key] = [];
    }
    this.events[key].push(func);
};
EventDispatcher.prototype.removeEventListener = function (key, func) {
    if (this.events.hasOwnProperty(key)) {
        for (var i in this.events[key]) {
            if (this.events[key][i] === func) {
                this.events[key].splice(i, 1);
            }
        }
    }
};
EventDispatcher.prototype.dispatchEvent = function (key, dataObj) {
    if (this.events.hasOwnProperty(key)) {
        dataObj = dataObj || {};
        dataObj.currentTarget = this;
        for (var i in this.events[key]) {
            this.events[key][i](dataObj);
        }
    }
};