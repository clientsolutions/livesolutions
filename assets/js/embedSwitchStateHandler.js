function EmbedSwitchStateHandler(embedHash, iframeId, currentState) {
	//good article:
	//http://javascriptissexy.com/oop-in-javascript-what-you-need-to-know/
	
	
	this.embedHash = embedHash;
	this.iframeId = iframeId;
	this.debug = true;
	this.configLocation = "http://clientsolutionsbucket.s3.amazonaws.com/switchAssets/" +embedHash +"/config.json";
	this.currentState = currentState;
	this.currentCount = 0;
	
	//http://vpc-clientsolutions01.newrow.com/liveSolutions/embedSwitch/state/pre/fffffffffffff
	
	//testing
	//this.stateUrlPrefix = "http://vpc-clientsolutions01.newrow.com/liveSolutions/embedSwitch/state/";
	
	//production
	this.stateUrlPrefix = "//csolcache.newrow.com/liveSolutions/embedSwitch/state/";
	
	this.checkTimer;
	this.checkTimerTime = 5000; //30 seconds = 30 000
	
	
	this.log = function(moduleThatIsLogging, textToLog) {
		if (this.debug === true ){
			console.log(moduleThatIsLogging + '::: ' +textToLog);
		}
	};
	
	
	this.startChecking = function() {
		this.log('checking','checking the state @ ' +this.configLocation);
		var thisStateHandler = this;
		this.checkTimer = window.setInterval(function() {
				thisStateHandler.fetchState();
				thisStateHandler.setCurrentCount();
				// if (thisStateHandler.currentCount >= 10) {
					// thisStateHandler.stopChecking();					
				// }
			}, this.checkTimerTime);
	};
	
	
	
	this.stopChecking = function() {
		this.log('checking', 'checking has stopped');
		window.clearInterval(this.checkTimer);
	};
	
	
	
	this.updateSwitchState = function(state) {
		var embedIframe = document.getElementById(this.iframeId);
		var newUrl = this.stateUrlPrefix +state +"/" +this.embedHash;
		this.log('updateState', 'updating the switch state to ' +state);
		this.log('updateState', 'updating the iframe url to ' +newUrl);
		
		embedIframe.src = newUrl;
		
		//set the current state to compare against subsequent requests
		this.currentState = state;
	};
	
	
	this.handleJsonReturn = function(data) {
		this.log('JSON_RETURN', "data_returned");
		this.log('JSON_RETURN', 'data.state = ' +data.state);
		
		if (data.state == this.currentState) {
			this.log('JSON_RETURN', 'no change, state is the same: ' +data.state);
		} else {
			this.updateSwitchState(data.state);
		}
		
	};
	
	
	this.fetchState = function() {
		this.log('fetching', 'I am checking the state of ' +this.configLocation);
		var thisStateHandler = this;
			
		$.ajax({
			  dataType: "json",
			  contentType: "application/json",
			  url: this.configLocation,
			  data: 'data',
			  success: function(data){thisStateHandler.handleJsonReturn(data);},
			  error: function(xhr, type, exception){thisStateHandler.log('fetching', type);}
		});
	};

	
	this.setCurrentCount = function() {
		this.log('count', 'updating the current count');
		this.currentCount ++;
		this.log('count', 'current count = '+ this.currentCount);
	};

	
	//let the world know you are alive
	this.log('construction', 'starting up with the check rate of : ' +this.checkTimerTime);
	this.log('construction', 'starting up with the embedHash: ' +embedHash);
	this.log('construction', 'starting up with the iframeId: ' +iframeId);
	this.log('construction', 'starting up with the currentState: ' +currentState);
};
