console.log('WSO::: wso_ loaded');
function Wso() {
	
	console.log('WSO::: creation of a new wso');
    this.meetings = [];
    this.node_servers ;
    this.proxy_servers = null;
    this.socket;
    this.debug = true;

    var that = this;
    $.get('/liveSolutions/assets/js/get_external_layers',function(data){
        that.node_servers = data;
        
        var number_of_nodes = data.length;
        var index = Math.floor(Math.random() * number_of_nodes);

        var socket = that.socket = io.connect(window.location.protocol +'//'+data[index].domain);        
        
        socket.on('connect', function () {
            if (this.debug && window.console) console.log('[wso.js] connected');
            socket.emit('get_storage_servers');
        });
        
        that.socket.on('set_storage_servers', function (data) {
            that.proxy_servers = data.servers_lists;
        });

        that.socket.on('update', function (result) {
            if (this.debug && window.console) console.log('[wso.js] go update: ',result);
            var meeting_id = result.data.id;
            that.meetings[meeting_id].callback(result);
        });    
    },'json');

}


Wso.prototype.add_cdr = function (type,data) {
    this.socket.emit('add_cdr', { type: type, data: data});
}

Wso.prototype.listen = function (meeting_id,path,callback) {
    // register only once the proxy servers are available
    if (this.proxy_servers == null) {
        var that = this;
        window.setTimeout(function(){
            that.listen(meeting_id,path,callback);
        },1000);
        return;
    }
    // setting proxyRoute
    window.proxyRoute = new ProxyRoute(wso.proxy_servers);
    this.meetings[meeting_id] = { callback: callback };
    this.socket.emit('listen', { meeting_id: meeting_id ,path:path});
}

Wso.prototype.leave_meeting = function (meeting_id) {
    this.socket.disconnect();//emit('disconnect');
}
