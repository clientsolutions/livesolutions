/**
* Will be in charge of switching and updating config
* requires jQuery 1.5 +
* requires jquery.xdomainrequest.min.js
*/
//http://clientsolutionsbucket.s3.amazonaws.com/switchAssets/[EMBED_HASH]/config.json

var DEBUG = false;
function stateHandler(client, environment, frameId, stateAlt) {
    'use strict';
    this.client = client;
    this.env = environment;
    this.productionConfig = client + '.config.json';
    this.testConfig = client + '.testconfig.json';
    this.currentState;
    this.contentId = frameId;
    this.customFrames = false; //Default
    if (typeof stateAlt !== 'undefined') {
      //Use custom slate/frame names
      // Assumes stateAlt is an array of strings
      this.customFrames = true;
      this.preFrame = stateAlt[0];
      this.liveFrame = stateAlt[1];
      this.postFrame = stateAlt[2];
    }

    this.run = function () {
        var self = this;
        self.getState(self.environment);
        this.interval = setInterval(function () {
            self.getState(self.environment);
        }, 30000); //every 30 seconds
    }

    this.run();
}

stateHandler.prototype.setState = function (state, environment) {
    'use strict';
}


stateHandler.prototype.getState = function () {
    'use strict';
    var getURL = 'http://clientsolutionsbucket.s3.amazonaws.com/switch/';
    if(this.env === 'production'){
      getURL += this.productionConfig;
    }else{
      getURL += this.testConfig;
    }
    var s3Inst = this;
    $.ajax({
      url: getURL,
      dataType: "json",
      contentType: "application/json",
      type: "GET",
      success: function(data){
        debugState(data);
        if( s3Inst.currentState === 'undefined' ){
          s3Inst.currentState = data.state;
        }else if( s3Inst.currentState === data.state ){
          //debugState('No Updates');
          return false;
        }else{
          s3Inst.currentState = data.state;
          s3Inst.updateSrc();
          //debugState('Changing Source to ' + s3Inst.currentState);
  			}
  		},
  		error: function(xhr, type, exception){
			debugState('jqXHR: '); 
			debugState(xhr);
			debugState('Type: ' + type);
			debugState('errorThrown ' + exception);
  		}
  	});
}

stateHandler.prototype.updateSrc = function () {
  'use strict';
  if(!this.customFrames){
    if(this.currentState === 'live'){
      document.getElementById(this.contentId).src = 'live.php';
    }else if(this.currentState === 'pre'){
      document.getElementById(this.contentId).src = 'pre.php';
    }else if(this.currentState === 'post'){
      document.getElementById(this.contentId).src = 'post.php';
    }
  }else {
    if(this.currentState === 'live'){
      document.getElementById(this.contentId).src = this.liveFrame;
    }else if(this.currentState === 'pre'){
      document.getElementById(this.contentId).src = this.preFrame;
    }else if(this.currentState === 'post'){
      document.getElementById(this.contentId).src = this.postFrame;
    }
  }
}

function debugState(msg) {
  if(DEBUG && window.console) {
    console.log(msg);
  }
}