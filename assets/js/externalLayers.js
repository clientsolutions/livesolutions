var external_layers = new Array();

var DEBUG = 'yurps';
switch (_newrowMPEconfig.NEWROW__CURRENT_STATE) {
	case 'pre':
		console.log('EXTERNAL_LAYER::: current state = pre');
		MEETING_ID = _newrowMPEconfig.NEWROW__PRE_MEETINGROOM;
		break;
	case 'live':
		console.log('EXTERNAL_LAYER::: current state = live');
		MEETING_ID = _newrowMPEconfig.NEWROW__LIVE_MEETINGROOM;
		break;
	case 'post':
		console.log('EXTERNAL_LAYER::: current state = post');
		MEETING_ID = _newrowMPEconfig.NEWROW__POST_MEETINGROOM;
		break;
	case 'vod':
		console.log('EXTERNAL_LAYER::: current state = vod');
		MEETING_ID = _newrowMPEconfig.NEWROW__VOD_MEETINGROOM;
		break;
	default:
		console.log('EXTERNAL_LAYER::: in default - no state matched');
		MEETING_ID = 'OMG';
		break;
}

console.log('EXTERNAL_LAYER::: MEETING_ID = ' + MEETING_ID);

//var MEETING_ID = getParam('meetingId');
// var STAGE_WIDTH = 640;
// var STAGE_HEIGHT = 360;
var STAGE_WIDTH = 896;
var STAGE_HEIGHT = 504;
// if (getParam('aspectRatio') == '4x3' ) {
	// STAGE_HEIGHT = 480;
// }
var embedHeight = 0;
var embedWidth = 0;

var BASE_STAGE_HEIGHT = 480;
var UPDATE_DELAY = 1000;

//var delay = getParam('exld') * 1000;
var delay = 0;
var update_timer_handler = false;

var container_width;
var container_height;


var layers_update_timers = {};

function add_external_layer(id,layer){
	console.log('EXTERNAL_LAYER::: adding a new layer = ('+id+') data=',layer);
	console.log('Adding new layer, id=('+id+') data=',layer);

	// check if the layer is empty
	if (typeof layer.layer_id == 'undefined') {
		console.log('EXTERNAL_LAYER::: layer is undefined, returning');
		return;	
	}

	// check wheter the layer shoul be displayed
	if (layer.name.indexOf('__clickable__') < 0) {
		if (id in layers_update_timers) {
			 delete layers_update_timers[id];
			  console.log('Layer '+id+' was deleted (missing __clickable__ prefix)');
		}
		return;
	}


	var ratio = layer.size / 100;
	var flash_width;
	var flash_height;

	console.log('EXTERNAL_LAYER::: ratio = ', ratio);
	console.log('EXTERNAL_LAYER::: container_width = ', container_width);
	console.log('EXTERNAL_LAYER::: container_height = ', container_height);
	
	if ((container_width / container_height) < (layer.width / layer.height)) {
		flash_width = container_width * ratio;	
		flash_height = flash_width * (layer.height / layer.width);
	} else {
		
		flash_height = container_height * ratio;
		flash_width = flash_height * (layer.width / layer.height); 							
	}
	
	console.log('EXTERNAL_LAYER::: base stage height = ' + BASE_STAGE_HEIGHT);
	console.log('EXTERNAL_LAYER::: container_height = ' + container_height);
	console.log('EXTERNAL_LAYER::: flash_width = ' + flash_width);
	console.log('EXTERNAL_LAYER::: flash_height = ' + flash_height);
	
	var new_layer_div = $('<div>',{id: id, class: 'layer'}).css({
		top: layer.top * (container_height / BASE_STAGE_HEIGHT )  , // bugfix - even on 16x9 use height of 480
		left: layer.left * (container_width / STAGE_WIDTH ) ,
		width: flash_width,
		height: flash_height,
		position: 'absolute'
	});
	
	console.log('EXTERNAL_LAYER::: new layer div =  = ', new_layer_div);
	
	if (typeof external_layers[id] == 'undefined' || !external_layers[id].on_air) {
		console.log('Hiding layer: defined=('+ (typeof external_layers[id] == 'undefined') +') on_air=('+external_layers[id].on_air+')');
		new_layer_div.css({visibility: 'hidden' });
		place_swf(new_layer_div,layer.path,id,true);
	} else {
		place_swf(new_layer_div,layer.path,id,false);	
	}
}

function place_swf(target,layer_path,id,hidden,timeout){
	
	console.log('EXTERNAL_LAYERS::: in place_swf');
	console.log('EXTERNAL_LAYERS::: target = ', target);
	console.log('EXTERNAL_LAYERS::: layer_path = ', layer_path);
	console.log('EXTERNAL_LAYERS::: id = ', id);
	console.log('EXTERNAL_LAYERS::: hidden = ', hidden);
	console.log('EXTERNAL_LAYERS::: timeout = ', timeout);
	
	timeout = timeout || 1;
	target.html($('<div>',{id: id+'_swf'}));
	$('.layers_container').append(target);
	
	

	var proxy = window.proxyRoute.get_next().domain;
	var url = window.location.protocol +'//' + proxy + layer_path;
	var flashvars = {};
	var params = { scale: "exactFit", wmode: "transparent" , allowscriptaccess: "always" };
	var attributes = {class:'flash' ,name: id+'_swf'};
	var callback = function(e){

		 //console.log('[[setTimeout]] typeof e.ref.PercentLoaded ==== '+typeof e.ref.PercentLoaded);
		layers_update_timers[id] = 'ready';

		//  window.setTimeout(function(){			
		//  console.log('[[setTimeout]] typeof e.ref.PercentLoaded ==== '+typeof e.ref.PercentLoaded);
		// layers_update_timers[id] = false;
		// 	 console.log('[[setTimeout]] typeof e.ref.PercentLoaded == '+typeof e.ref.PercentLoaded);
		// 	if (typeof e.ref.PercentLoaded !== 'function' || e.ref.PercentLoaded() == 0) {				
		// 		$('#'+id+'_swf').remove();
		// 		place_swf(target,layer_path,id,hidden,timeout * 2);
		// 	} else {
		// 		layers_update_timers[id] = false;
		// 	}
		 // },timeout * 1000);
	};
	var flashvars = {};
	var params = { scale: "exactFit", wmode: "transparent" , allowscriptaccess: "always" };
	var attributes = {class:'flash' ,name: id+'_swf'};
	var callback = '';
	swfobject.switchOffAutoHideShow();
	swfobject.embedSWF(url, id+'_swf', "100%", "100%", "9.0.0","expressInstall.swf", flashvars, params, attributes,callback);
}

function update_external_layer(id,layer){

	if (!(id in layers_update_timers)) {
		 console.log('update_external_layer layer not ready yet, sleeping .....');
		window.setTimeout(function(){
			update_external_layer(id,layer)			
		},1000);
		return;
	}
	if (layers_update_timers[id] == 'loading') {
		 console.log('update_external_layer layer still loading, sleeping .....');
		window.setTimeout(function(){
			update_external_layer(id,layer)			
		},1000);
		return;
	}
	
	if (layers_update_timers[id] == 'deleted') {
		 console.log('update_external_layer layer was deleted, quiting .....');
		return;
	}
	

    if (update_timer_handler != false) {
    	 console.log('update_external_layer clearing timer');
        clearTimeout(update_timer_handler);
        update_timer_handler = false;
    }

    

    // delay actions to avoid many cnages in a short time
    update_timer_handler = window.setTimeout(function(){
    	 console.log('update_timer_handler fired with id="'+id+'"');
		$('#'+id).addClass("_remove_");
		$('#'+id+'_swf').addClass("_remove_");
		$('._remove_').remove();
		
		 console.log('==============> ADDING LAYERS (UPDATE)');
		add_external_layer(id,layer);
		
    },UPDATE_DELAY);
}


function init_external_layers(player_id) {
	console.log('EXTERNAL_LAYER::: in init_external_layers');
	
	if ($('#'+player_id).width() == 0){
		// fix for firefox - start when player was loaded
		window.setTimeout(function(){
			init(player_id)
		},100);
		return;
	}
	
	console.log('EXTERNAL_LAYER::: calling place_container with the player_id of '+ player_id);
	place_container(player_id);
	
	console.log('EXTERNAL_LAYER::: creating a new wso');
	var wso = window.wso = new Wso();
	
	 console.log('Listening to meeting ',MEETING_ID);
	wso.listen(MEETING_ID, 'layers', function(result){
		 console.log('new update:',result);
		if (delay < 1) {
			process_request(result);
		} else {
			window.setTimeout(function(){
				process_request(result);
			},delay);			
		}
	});
}

function process_request(result){
	console.log('EXTERNAL_LAYER::: in process request with a param of ', result);
	switch(result.data.type) {
    	case 'full':
			for (var id in result.data.layers) {
				var layer = result.data.layers[id];
				console.log('EXTERNAL_LAYER::: layer = ', layer);
				
				external_layers[id] = layer;
				console.log('EXTERNAL_LAYER::: external_layers = ', external_layers);
				
				 console.log('==============> ADDING LAYERS (FULL)');
				add_external_layer(id,layer);
			}
    		break;
    	case 'single_layer': 
    		for (var id in result.data.layers) {
				var layer = result.data.layers[id];

				// update layer 
				external_layers[id].content_id = layer.content_id;
				external_layers[id].layer_id = layer.layer_id;
				external_layers[id].path = layer.path;

				var changed = false;
				if (external_layers[id].left !== layer.left) {
					external_layers[id].left = layer.left;	
					changed = true;
				}				
				if (external_layers[id].top !== layer.top) {
					external_layers[id].top = layer.top;	
					changed = true;
				}
				
				if (external_layers[id].size !== layer.size) {
					external_layers[id].size = layer.size;	
					changed = true;
				}

				if (external_layers[id].name !== layer.name) {
					external_layers[id].name = layer.name;	
					changed = true;
				}
				
				if (id in layers_update_timers) {
					if (changed) {
						 console.log('update_external_layer('+id+')');
						update_external_layer(id,layer);
					} else {
						 console.log('layer was not changed, nothing to do...');
					}
				} else {
					 console.log('add_external_layer('+id+')');
					 console.log('==============> ADDING LAYERS (SINGLE)');
					layers_update_timers[id] = 'loading';
					add_external_layer(id,layer);
				}
    		}
    		break;
		case "layers_so":
			// remove removed
			for (var id in external_layers) {
				if (!(id in result.data.layers)) {
					 console.log('removing deleted layer ('+id+')');
					$('#'+id).remove();
					delete external_layers[id];
					layers_update_timers[id] = 'deleted';
				} 
			}
			// hide hidden
			for (var id in result.data.layers) {
				 console.log('id ('+id+') type= '+(typeof external_layers[id]));
				if (typeof external_layers[id] == 'undefined') {
					 console.log('==============> ADDING LAYERS (EMPTY)');
					 external_layers[id] = {};
				}
				var layer = result.data.layers[id];
				if (layer.on_air) {
					external_layers[id].on_air = true;
					$('#'+id).css({visibility:'visible'});
				} else {
					external_layers[id].on_air = false;
					$('#'+id).css({visibility:'hidden'});
				}
			}
			break;
    }
     console.log("After update: external_layers = ",external_layers);

}


function place_container(video_id){
	console.log('EXTERNAL_LAYERS::: in place_container, video ID = ' +video_id);
	var videoPlayerLayer = $('#'+video_id); 
	//videoPlayerLayer.css({backgroundColor: '#ffffff'});
	console.log('EXTERNAL_LAYERS::: in videoplayerLayer = ', videoPlayerLayer);
	
	var video_width = $('#'+video_id).width();
	var video_height = $('#'+video_id).height();
	
	$('#'+video_id).after($('<div>',{class:'layers_container'}).css({position: 'absolute', overflow: 'hidden'}));
	console.log('EXTERNAL_LAYERS::: should have placed a div with the class of layers_container here.');
	
	
	var top;
	if ((STAGE_WIDTH / STAGE_HEIGHT) > (video_width / video_height)) {
		// height bigger
		console.log('EXTERNAL_LAYERS::: in height bigger test result');
		container_width = video_width;
		container_height = video_width * STAGE_HEIGHT / STAGE_WIDTH;
		$('.layers_container').css({
			width: container_width,
			height: container_height,
			top: (video_height - (video_width * STAGE_HEIGHT / STAGE_WIDTH)) / 2
		});
	} else {
		// width bigger
		console.log('EXTERNAL_LAYERS::: in width bigger test result');
		container_width = video_height * STAGE_WIDTH  / STAGE_HEIGHT;
		console.log('EXTERNAL_LAYERS::: layers_container containter width = '+container_width);
		
		container_height = video_height;
		console.log('EXTERNAL_LAYERS::: layers_container containter height = '+container_height);
		$('.layers_container').css({
			width: container_width ,
			height: container_height,
			left: (video_width - (video_height * STAGE_WIDTH  / STAGE_HEIGHT )) / 2
		});
	}
	
	console.log('EXTERNAL_LAYER::: video_width = ', video_width);
	console.log('EXTERNAL_LAYER::: video_height = ', video_height);
	console.log('EXTERNAL_LAYER::: STAGE_WIDTH = ', STAGE_WIDTH);
	console.log('EXTERNAL_LAYER::: STAGE_HEIGHT = ', STAGE_HEIGHT);
	
}


function add_layer_CDR(id){
	var id = $('#'+id).parent().attr('id');
	wso.add_cdr('external_layer_click',{
		layer_id: external_layers[id].layer_id,
		meeting_id: MEETING_ID}
	);

	// add 3rd eye record
	var record = {
		app: 'mpe',
		mod: 'cl',
		t : 'clickableLayer',
		st : id,
		mid : MEETING_ID,
		camp: 'HP',
		ua : navigator.userAgent,
		hs: 'www.walmart.com'
	};
	statObj.sendPing(record);
}
