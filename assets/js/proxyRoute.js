
var ProxyRoute = function ProxyRoute(servers) {
    this.proxy_servers = [];
    this.index = -1;
    if (typeof servers.satellite !== 'undefined') {
        suffle_array(servers.satellite,this.proxy_servers)
    }
    suffle_array(servers.head_center,this.proxy_servers)
}

function suffle_array(source,target){
    var length = source.length;   
    var offset = Math.floor(Math.random() * length);
    for (var i = 0 ; i < length; i++) {
        target.push(source[(i+offset) % length]);
    }
}

ProxyRoute.prototype.get_next = function () {
    this.index = (this.index + 1) % this.proxy_servers.length;
    return this.proxy_servers[this.index];
}