function LayerConfigHandler(hash, stateIframeHolder) {
	
	console.log('layers::: creating a new layerconfig handler');
	
	this.debug = 'yurps';
	
	this.embedHash = hash;
	this.stateIframeHolderId = stateIframeHolder;
	this.configLocation = "http://clientsolutionsbucket.s3.amazonaws.com/switchAssets/" +hash +"/layerConfig.json";
	
	this.LAYER_ID_PREFIX = 'newrow__' +this.embedHash +'-';
	
	this.checkTimer;
	this.checkTimerTime = 5000; //30 seconds = 30 000
	this.layersArray = Array();
	
	
	this.log = function(moduleThatIsLogging, textToLog) {
		if (this.debug === true ){
			console.log(moduleThatIsLogging + '::: ' +textToLog);
		}
	};
	
	
	this.startChecking = function() {
		this.log('LAYERS - start checking','checking the state @ ' +this.configLocation);
		var thisLayerHandler = this;
		this.checkTimer = window.setInterval(function() {
				thisLayerHandler.fetchState();
			}, this.checkTimerTime);
	};
	
	
	this.fetchState = function() {
		this.log('LAYERS - fetching', 'I am checking the state of ' +this.configLocation);
		var thisLayerHandler = this;
			
		$.ajax({
			  dataType: "json",
			  contentType: "application/json",
			  url: this.configLocation,
			  data: 'data',
			  success: function(data){thisLayerHandler.handleJsonReturn(data);},
			  error: function(xhr, type, exception){thisLayerHandler.log('fetching', type);}
		});
	};
	
	
	this.handleJsonReturn = function(data) {
		this.log('LAYER JSON_RETURN', "data_returned");
		this.log('LAYER JSON_RETURN', 'data = ' +data);
		
		
		if (this.layersArray.length > 0) {
			//loop through each layer (item in the array)
			//check the layers 
			for (var i = 0; i < data.length; i++) {
				console.log('LAYERS -handleJsonReturn::: returned looping through layer ' + i);
				
				var layerExists = 'no';
				
				for(var c=0; c < this.layersArray.length; c++) {
					console.log('LAYERS::: this.layers = ', this.layersArray);
					//console.log('LAYERS::: checking the layer id of ', this.layersArray[c].id);
					
					if (this.layersArray[c].id === data[i].id) {
						console.log('LAYERS::: this layer exists, layerID = ' +this.layersArray[c].id);
						layerExists = 'yes';
						
						console.log('LAYERS:::  current live param = ', this.layersArray[c].live);
						console.log('LAYERS:::  setting the live param to ', data[i].live);
						
						this.layersArray[c].live = data[i].live;
						
						console.log('LAYERS:::  live param now = ', this.layersArray[c].live);
					}
				}
				////////////////////////////////////////////////////////
				//adding a new layer to the array
				////////////////////////////////////////////////////////
				if (layerExists === 'no') {
					this.addNewLayer(data[i]);
				}
				
			}
		}
		else {
			////////////////////////////////////////////////////////
			//initial fetch of the layers config object
			////////////////////////////////////////////////////////
			console.log('LAYERS::: this is the first fetch ');
			this.handleInitialFetchData(data);
		}
		
		console.log("LAYERS::: current layer array = ", this.layersArray);	
		
		//now loop through all of the layers and turn them 'on' or 'off'
		this.setLayerVisibility();
		
	};
	
	
	this.addNewLayer = function(thisItem) {
		thisItem.showing = 'no';
		thisItem.layerID = this.LAYER_ID_PREFIX +layerData.id;
		
		this.layersArray.push(thisItem);
		console.log('LAYERS::: adding layer '+ thisItem.id);
	};
	
	
	this.handleInitialFetchData = function(incomingData) {
		this.layersArray = incomingData;
		console.log('LAYERS::: this.layersArray = ', this.layersArray);
		
		//add the 'showing' parameter and the layerID parameter
		for(var c=0; c<this.layersArray.length; c++) {
			this.layersArray[c].showing = 'no';
			this.layersArray[c].layerID = this.LAYER_ID_PREFIX +this.layersArray[c].id;
		}
	};
	
	this.setLayerVisibility = function() {
		for(var c=0; c < this.layersArray.length; c++) {
			console.log('LAYERS::: this layer showing = ' + this.layersArray[c].showing);
			console.log('LAYERS::: this layer live = ' + this.layersArray[c].live);
			if ((this.layersArray[c].live == 1) && (this.layersArray[c].showing == 'no')) {
				//show it
				this.placeLayer(this.layersArray[c]);
			}
			
			if ((this.layersArray[c].live == 0) && (this.layersArray[c].showing == 'yes')) {
				this.removeLayer(this.layersArray[c]);
			}
		}
	};
	
	
	this.placeLayer = function(layerData) {
		//cheat: http://stackoverflow.com/questions/10619445/the-prefered-way-of-creating-a-new-element-with-jquery
		layerData.showing = 'yes';
		console.log('LAYERS::: placing the layer for the data ', layerData);
		var layerHolder = $("#" +this.stateIframeHolderId);
		var layerDiv = $("<div>", {id: layerData.layerID, class: 'layerClass'});
		layerDiv.css({"width":layerData.width, "position":'absolute', "height":layerData.height, "top":layerData.yPos+'px', "left":layerData.xPos+'px'}); //for testign: "background-color": "#666666"
		layerHolder.append(layerDiv);
		
		var innerHTML;
		
		if (layerData.contentType == 'image') {
			innerHTML = "<img src=" +layerData.contentUrl +">";
		}
		if (layerData.contentType == 'swf') {
			console.log('LAYERS::: SWF placement ', layerData);
			innerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' +layerData.width +'" height="' + layerData.height +'">';
			innerHTML +='<param name="movie" value="' +layerData.contentUrl +'" />';
			innerHTML +='<param name="scale" value="exactFit" />';
			innerHTML +='<param name="wmode" value="transparent" />';
			innerHTML +='<param name="allowscriptaccess" value="always" />';
			innerHTML +='<!--[if !IE]>-->';
			innerHTML +='<object type="application/x-shockwave-flash" data="' +layerData.contentUrl +'" width="' +layerData.width +'" height="' + layerData.height +'">';
		    innerHTML +='<!--<![endif]-->';
		    innerHTML +='<p>there should be a swf here</p>';
		    innerHTML +='<!--[if !IE]>-->';
		    innerHTML +='</object>';
		    innerHTML +='<!--<![endif]-->';
		    innerHTML +='</object>';
		}
		
		layerDiv.html(innerHTML);
		//alert('placing layer on ' +layerHolder);
		
		if(layerData.clickthroughUrl) {
			layerDiv.click(function(){ window.open(layerData.clickthroughUrl); });
		}
		
	}
	
	this.removeLayer = function(layerData) {
		console.log('LAYERS::: removing the layer for the data ', layerData);
		$("#"+layerData.layerID).remove();
		layerData.showing = 'no';
	}
	
	console.log('LAYERS - initting::: starting up');
}