<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>newrow_ BackStage</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" />
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
  	<a class="navbar-brand" href="/liveSolutions/">newrow_<br />BackStage</a>
  	<ul class="nav navbar-nav">
  		<li class="active">
			<a href="/liveSolutions/embedSwitch/">
				<span class="label text-uppercase" style="color: #000000;">
					switch main
				</span>
			</a>
		</li>
    <li>
      <a href="/liveSolutions/poll/">
        <span class="label text-uppercase" style="color: #000000;">
          polls
        </span>
      </a>
    </li>
     <li>
      <a href="/liveSolutions/Layers/">
        <span class="label text-uppercase" style="color: #000000;">
          layers
        </span>
      </a>
    </li>
	</ul>
	<p class="navbar-text navbar-right"><a href="/liveSolutions/auth/logout" class="navbar-link">Sign Out</a></p>
  </div>
</nav>	
<!-- end of the header -->
<!-- spacer -->
<div class="" style="height: 80px;"></div>