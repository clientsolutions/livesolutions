<?php

// echo "<pre>";
	// print_r($switchInfo);
// echo "</pre>";


//Check if forced state
$noStateCheck = FALSE;

if(! isset($_GET['state']) ){
  //Process and choose 'correct' state
  //$jsonGet = file_get_contents('http://clientsolutionsbucket.s3.amazonaws.com/switch/hp.testconfig.json');
  //$jsonGet = file_get_contents('http://clientsolutionsbucket.s3.amazonaws.com/switch/hp.config.json');
  //$jsonArray = json_decode($jsonGet);
  
  $stateToLoad = $switchInfo[0]->current_state;
  
}else{
  //Force state
  $stateToLoad = $_GET['state'];
  $noStateCheck = TRUE;
}

$iframeUrl = '//csolcache.newrow.com/liveSolutions/embedSwitch/state/' .$stateToLoad .'/' .$switchInfo[0]->lookup_hash;
$width = $switchInfo[0]->width;
$height = $switchInfo[0]->height;

// echo "<br/>";
// echo "<br/>";
// echo "width = $width";
// echo "<br/>";
// echo "height = $height";
// echo "<br/>";
// echo "iframeUrl = $iframeUrl";

?>
<!doctype html>
<html lang="en">
<head>
  <!-- <link rel="stylesheet" href="../style.css" /> -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Content</title>
</head>
<body style="margin: 0px;">
	<div id="experienceHolder">
		<div id='stateContent' class="content" style="position: absolute;">
			<iframe id="stateIframeHolder" src="<?php echo $iframeUrl; ?>" scrolling="no" width="<?php echo($width); ?>" height="<?php echo($height); ?>" frameborder="no">Your browser does not support iframes</iframe>
		</div>
	<div>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  
  <?php if(!$noStateCheck): ?>
  <script type="text/javascript">
  $(document).ready(function(){
    var hpData = {mod: 'sw' , camp: 'SHORTY'}
    //turn this back on for the state
    // var a = new thirdEye(hpData);
    //var brain = new stateHandler('hp', 'production', 'stateIframeHolder', ['vod.php', 'mpe.php', 'vod2.php']);
  });
  </script>
  <?php endif; ?>
  
  <script src="//csolcache.newrow.com/liveSolutions/assets/js/embedSwitchStateHandler.js"></script>
  <script src="//csolcache.newrow.com/liveSolutions/assets/js/LayerConfigHandler.js"></script>
  <script type="text/javascript">
  	$(document).ready(function(){
  			//start checking for state updates
  			var stateChecker = new EmbedSwitchStateHandler('<?php echo($switchInfo[0]->lookup_hash); ?>', 'stateIframeHolder', '<?php echo($switchInfo[0]->current_state); ?>');
			stateChecker.startChecking();
			
			//start checking for layer updates
			var layerChecker = new LayerConfigHandler('<?php echo($switchInfo[0]->lookup_hash); ?>', 'experienceHolder');
			layerChecker.startChecking();
			console.log('layers ::: I should be checking for the state');
  		}
  	);
  </script>
  
  <script type="text/javascript">
  var iframe = document.getElementById('stateIframeHolder');
  var messageHandle = function(evt){
    //Relay message to parent and child
    stateIframeHolder.contentWindow.postMessage(evt.data, '*');//Send to child
    parent.postMessage(evt.data, '*');//Send to parent
  }
  </script>
  
  <!-- event handler -->
  <!-- is this for the live layers? -->
  <script type="text/javascript">
  // Create IE + others compatible event handler
  var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
  var eventer = window[eventMethod];
  var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

  // Listen to message from child window
  eventer(messageEvent,function(e) {
    if(e.origin === 'http://clientsolutions.watchitoo.com' || e.origin === 'http://csolcache.watchitoo.com' || e.origin === 'http://localhost') {
      messageHandle(e);
    }
  },false);
  </script>
  
  <!-- google analytics -->
  <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5062200-1']);
  _gaq.push(['_trackPageview']);

  (function() {
  var ga = document.createElement('script');
  ga.type = 'text/javascript';
  ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(ga, s);
  })();
  </script>
</body>
</html>