<h1 style="padding-top: 50px;">all the switches</h1>
<a class="btn btn-default" href="newSwitch/" role="button">New Embed Switch</a><br /><br />
<?php foreach ($embeds as $thisEmbed) { ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo $thisEmbed->title; ?> <span style="color: grey;">(<?php echo$thisEmbed->lookup_hash; ?>)</span></h3>
		</div>
		<div class="panel-body">
			<a class="btn btn-default" href="edit/<?php echo $thisEmbed->id; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> edit this switch embed</a>
			<a class="btn btn-default" href="code/<?php echo $thisEmbed->id; ?>"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span> get the code for this switch embed</a>
			<a class="btn btn-info" href="viewSwitchState/<?php echo $thisEmbed->id; ?>"><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span> set the current live state of this switch</a>
		</div>
	</div>
<?php } ?>