<?php
/**
*	Switch
* uses the new s3conf.class.php --> standard for all future switches
*	
*/
$client = '';
$env = 'testing';
if(isset($_GET['c'])) {
	$client = strtolower($_GET['c']);
	$env = (isset($_GET['e'])) ? strtolower($_GET['e']) : 'testing'; //testing || production
}

?>
<!doctype html>
<html>
<head>
	<title>State Switch</title>
</head>
<body>
	<div class="">
		<input id="client" type="text" placeholder="Client" value="<?php echo $client; ?>">
		<select id="environment">
			<option value="test" selected>Testing</option>
			<option value="production">Production</option>
		</select>
		<button class="switch">Pre</button>
		<button class="switch">Live</button>
		<button class="switch">Post</button>
		<div id="message"></div>
	</div>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<!--script type="text/javascript" src="class/s3.js"></script-->
	<script type="text/javascript">
	$(document).ready(function(){
		
		$('.switch').click(function(){
			var state = $( this ).html().toLowerCase();
			var environment = $('#environment :selected').val();
			var client = $( '#client' ).val().toLowerCase();
			updateConfig(client, state, environment);
		})

		function updateConfig(client, state, environment){
			//Assume state and environment not blank, due to forcing values
			console.log("Sending: "+client+' '+state+' '+environment);
			$.ajax({
				url: 'ajax.php',
				method: 'PUT',
				data: {"client" : client, "state" : state, "environment" : environment}
			}).done(function(data, textStatus, jqXHR){
				console.log(data);
				updateMessage('Set state to '+ state + ' for '+ client + ' on '+environment);
			}).fail(function(jqXHR, textStatus, errorThrown){
				console.log('jqXHR: '+ jqXHR);
				console.log('textStatus: '+textStatus);
				console.log('errorThrown: '+errorThrown);
			});
		}

		function updateMessage(msg){
			$('#message').html('');
			$('#message').html(msg);
		}

	});
	</script>
</body>
</html>