<h1 style="padding-top: 50px;">create a new embed switch code for <?php echo($group[0]->name); ?></h1>
<?php echo form_open('EmbedSwitch/newSwitch'); ?>
	<div style="padding-left: 10px;" class="form-group">
    	<label for="embedSwitchTitle">Embed Switch Title</label>
    	<input type="text" name="esTitle" class="form-control" id="embedSwitchTitle" placeholder="Please Give This Embed Switch a Title">
    </div>
    <div class="form-inline">
	<div style="padding-left: 10px;" class="form-group">
		<label for="embedSwitchTitle">Embed Switch Width</label>
		<input type="text" size="10" name="esWidth" class="form-control" id="embedSwitchWidth" placeholder="Width">
		<label style="padding-left: 10px;" for="embedSwitchTitle">Embed Switch Height</label>
		<input type="text" size="10" name="esHeight" class="form-control" id="embedSwitchHeight" placeholder="Height">
		<input type="hidden" name="group" value="<?php echo($group[0]->id); ?>">
	</div>
	</div>
	<div style="padding-left: 10px;"  class="form-group" style="padding-top: 10px;">
		<button type="submit" class="btn btn-default">Create</button>
	</div>
</form>