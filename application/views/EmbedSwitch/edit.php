<h1 style="padding-top: 50px;"><?php echo $embeds[0]->title; ?></h1>
<div role="tabpanel">

<!-- IMAGE save success and error messages -->
<?php if (isset($_GET['upload']) && $_GET['upload'] == 'error') { ?>
        <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <?php echo $_GET['errors']; ?>
        </div>


<?php } else if (isset($_GET['upload']) && $_GET['upload'] == 'success') { ?>
        <div class="alert alert-success" role="alert">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                <span class="sr-only">Success:</span>
                Your upload completed
        </div>
<?php } ?>

<!-- CODE save success and error messages -->
<?php if (isset($_GET['codeSave']) && $_GET['codeSave'] == 'error') { ?>
        <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <?php echo $_GET['errors']; ?>
        </div>


<?php } else if (isset($_GET['codeSave']) && $_GET['codeSave'] == 'success') { ?>
        <div class="alert alert-success" role="alert">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                <span class="sr-only">Success:</span>
                Your code was saved successfully
        </div>
<?php } ?>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#pre" aria-controls="pre" role="tab" data-toggle="tab">Pre-state</a></li>
    <li role="presentation"><a href="#live" aria-controls="live" role="tab" data-toggle="tab">Live-state</a></li>
    <li role="presentation"><a href="#post" aria-controls="post" role="tab" data-toggle="tab">Post-state</a></li>
    <li role="presentation"><a href="#vod" aria-controls="vod" role="tab" data-toggle="tab">VOD-state</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
        <!------------------------------------------------------------->
        <!-- PRE PANEL -->
        <!------------------------------------------------------------->
    <div role="tabpanel" class="tab-pane fade in active" id="pre">
        <?php echo $embeds[0]->pre_state; ?>
        <label for="currentPreCode"></label>
        <textarea name="currentPreCode" rows="3" class="form-control"><?php echo $embeds[0]->pre_state; ?></textarea>
        <!-- buttons and their respective modals -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".pre-code">Insert Code</button>

                <div class="modal fade bs-example-modal-lg pre-code" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                                        <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                                                <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 id="mySmallModalLabel" class="modal-title">Add your own code</h4>
                                </div>
                        <?php echo form_open('EmbedSwitch/saveData/' .$embeds[0]->id .'/pre'); ?>
                                <textarea name="code" class="form-control" rows="3"></textarea>
                                <input type="hidden" name="return_to" value="EmbedSwitch/edit/<?php echo $embeds[0]->id; ?>" />
                                <button type="submit" class="btn btn-default">Save</button>
                        </form>
                    </div>
                  </div>
                </div>


                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".pre-image">Upload Image</button>

                <div class="modal fade bs-example-modal-sm pre-image" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                                <div class="modal-header">
                                        <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                                                <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 id="mySmallModalLabel" class="modal-title">Choose an Image</h4>
                                </div>
                        <?php echo form_open_multipart('upload/do_upload/' .$embeds[0]->id .'/'); ?>
                                <input type="file" name="pre_upload" size="20" />
                                <br /><br />
                                <input class="btn btn-primary" type="submit" value="upload">
                                <input type="hidden" name="return_to" value="EmbedSwitch/edit/<?php echo $embeds[0]->id; ?>" />
                                <input type="hidden" name="state" value="pre" />
                        </form>
                    </div>
                  </div>
                </div>
    </div>
    <!------------------------------------------------------------->
    <!-- LIVE PANEL -->
    <!------------------------------------------------------------->
    <div role="tabpanel" class="tab-pane fade" id="live">
        <?php echo $embeds[0]->live_state; ?>
        <label for="currentLiveCode"></label>
        <textarea name="currentLiveCode" rows="3" class="form-control"><?php echo $embeds[0]->live_state; ?></textarea>
                <!-- buttons and their respective modals -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".live-code">Insert Code</button>

                <div class="modal fade bs-example-modal-lg live-code" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                                        <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                                                <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 id="mySmallModalLabel" class="modal-title">Add your own code</h4>
                                </div>
                        <?php echo form_open('EmbedSwitch/saveData/' .$embeds[0]->id .'/live'); ?>
                                <textarea name="code" class="form-control" rows="3"></textarea>
                                <input type="hidden" name="return_to" value="EmbedSwitch/edit/<?php echo $embeds[0]->id; ?>" />
                                <button type="submit" class="btn btn-default">Save</button>
                        </form>
                    </div>
                  </div>
                </div>

    </div>
    <!------------------------------------------------------------->
    <!-- POST PANEL -->
    <!------------------------------------------------------------->
	<div role="tabpanel" class="tab-pane fade" id="post">
        <?php echo $embeds[0]->post_state; ?>
        <label for="currentPostCode"></label>
        <textarea name="currentPostCode" rows="3" class="form-control"><?php echo $embeds[0]->post_state; ?></textarea>
        <!-- buttons and their respective modals -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".post-code">Insert Code</button>

                <div class="modal fade bs-example-modal-lg post-code" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                                        <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                                                <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 id="mySmallModalLabel" class="modal-title">Add your own code</h4>
                                </div>
                        <?php echo form_open('EmbedSwitch/saveData/' .$embeds[0]->id .'/post'); ?>
                                <textarea name="code" class="form-control" rows="3"></textarea>
                                <input type="hidden" name="return_to" value="EmbedSwitch/edit/<?php echo $embeds[0]->id; ?>" />
                                <button type="submit" class="btn btn-default">Save</button>
                        </form>
                    </div>
                  </div>
                </div>


                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".post-image">Upload Image</button>

                <div class="modal fade bs-example-modal-sm post-image" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                                <div class="modal-header">
                                        <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                                                <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 id="post_mySmallModalLabel" class="modal-title">Choose an Image</h4>
                                </div>
                        <?php echo form_open_multipart('upload/do_upload/' .$embeds[0]->id .'/'); ?>
                                <input type="file" name="pre_upload" size="20" />
                                <br /><br />
                                <input class="btn btn-primary" type="submit" value="upload">
                                <input type="hidden" name="return_to" value="EmbedSwitch/edit/<?php echo $embeds[0]->id; ?>" />
                                <input type="hidden" name="state" value="post" />
                        </form>
                    </div>
                  </div>
                </div>
    </div>
    <!------------------------------------------------------------->
    <!-- VOD PANEL -->
    <!------------------------------------------------------------->
    <div role="tabpanel" class="tab-pane fade" id="vod">
        <?php echo $embeds[0]->vod_state; ?>
        <label for="currentVodCode"></label>
        <textarea name="currentVodCode" rows="3" class="form-control"><?php echo $embeds[0]->vod_state; ?></textarea>
        <!-- buttons and their respective modals -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".vod-code">Insert Code</button>

                <div class="modal fade bs-example-modal-lg vod-code" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                                        <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                                                <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 id="mySmallModalLabel" class="modal-title">Add your own code</h4>
                                </div>
                        <?php echo form_open('EmbedSwitch/saveData/' .$embeds[0]->id .'/vod'); ?>
                                <textarea name="code" class="form-control" rows="3"></textarea>
                                <input type="hidden" name="return_to" value="EmbedSwitch/edit/<?php echo $embeds[0]->id; ?>" />
                                <button type="submit" class="btn btn-default">Save</button>
                        </form>
                    </div>
                  </div>
                </div>
    </div>
  </div>
</div>