<?php
	// echo('<pre>');
		// print_r($layer);
	// echo('</pre>');
	
	// echo('<pre>');
		// print_r($switch);
	// echo('</pre>');
	
	//set the embed
	//create the layer on top of it
	//
	$states = array('pre','live', 'post', 'vod');
	$stateClasses = array();
	
	foreach ($states as $thisState) {
		if ($currentSwitchState == $thisState) {
			$stateClasses[$thisState]['label'] = 'label-success';
			$stateClasses[$thisState]['li'] = 'active';
		}
		else {
			$stateClasses[$thisState]['label'] = 'label-info';
			$stateClasses[$thisState]['li'] = '';
		}
}

?>
<!-- how to jquery -->
<!-- https://api.jqueryui.com/draggable/#entry-examples -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="//apps.newrow.com/js/swfobject.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<nav class="navbar navbar-default navbar-fixed-top" style="margin-top: 51px;">
  <div class="container">
  	<a class="navbar-brand" href="#">Upload an Asset</a>
	<!-- upload a new visual asset button -->
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".layer-asset">Choose Visual Asset</button>
    
    <!-- end of the new visual asset upload button -->
  </div>
</nav>
<nav class="navbar navbar-default navbar-fixed-top" style="margin-top: 102px;">
  <div class="container">
  	<a class="navbar-brand" href="#">Preveiw On State</a>
  	<ul class="nav navbar-nav">
  		<?php foreach ($states as $thisState) { ?>
			<li class="<?php echo $stateClasses[$thisState]['li']; ?>">
				<a href="/liveSolutions/Layers/preview/<?php echo($layer[0]->id); ?>/<?php echo $thisState; ?>">
					<span class="label text-uppercase  <?php echo $stateClasses[$thisState]['label']; ?>">
						<?php echo $thisState; ?>
					</span>
				</a>
			</li>	  
		<?php } ?>
	</ul>
  </div>
</nav>
<nav class="navbar navbar-default navbar-fixed-top" style="margin-top: 153px;">
  <div class="container">
  	<a class="navbar-brand" href="#">Position:</a>
  	<?php echo form_open('Layers/savePosition'); ?>
    <div class="form-inline">
		<div style="padding-left: 10px; padding-top: 10px;" class="form-group">
			<label for="layerX">X</label>
			<input type="text" size="10" name="layerX" class="form-control" id="layerX" value="<?php echo($layer[0]->xPos); ?>">
			<label style="padding-left: 10px;" for="layerY">Y</label>
			<input type="text" size="10" name="layerY" class="form-control" id="layerY" value="<?php echo($layer[0]->yPos); ?>">
			<input type="hidden" name="layerId" value="<?php echo($layer[0]->id); ?>">
			<input type="hidden" name="previewState" value="<?php echo($currentSwitchState); ?>">
			<button type="submit" class="btn btn-default">Save Position</button>
		</div>
	</div>
</form>
			
<?php echo form_open('Layers/saveUrl'); ?>
    <div class="form-inline">
		<div style="padding-left: 10px; padding-top: 10px;" class="form-group">
			<label for="clickthroughUrl">Clickthrough</label>
			<input type="text" size="50" name="clickthroughUrl" class="form-control" id="clickthroughUrl" value="<?php echo($layer[0]->clickthroughUrl); ?>">
			<input type="hidden" name="layerId" value="<?php echo($layer[0]->id); ?>">
			<input type="hidden" name="previewState" value="<?php echo($currentSwitchState); ?>">
			<button type="submit" class="btn btn-default">Save Clickthrough</button>
		</div>
	</div>
</form>
  </div>
</nav>
<div id="experienceHolder" style="margin-left:10px; margin-top: 173px; position: relative; width:<?php echo($switch[0]->width); ?>px; height: <?php echo($switch[0]->height); ?>">
	<!-- iframe holder  -->
	<div id='iframeHolder' style="position: absolute;" >
		<iframe id="stateIframeHolder" src="//csolcache.newrow.com/liveSolutions/embedSwitch/state/<?php echo($currentSwitchState); ?>/<?php echo($switch[0]->lookup_hash); ?>" scrolling="no" width=" <?php echo($switch[0]->width); ?>" height=" <?php echo($switch[0]->height); ?>" frameborder="no">Your browser does not support iframes</iframe>
	</div>
	<!-- layerDiv  -->
	<?php if ($layer[0]->contentUrl) { ?>
		<a style="cursor: default" href="<?php echo($layer[0]->clickthroughUrl); ?>" target="_blank">
		<div id="layerPreviewHolder" class="draggable" style="display: block; left:<?php echo($layer[0]->xPos); ?>px; top:<?php echo($layer[0]->yPos); ?>px; position: absolute;">
			<?php
				if ($layer[0]->contentType == 'image') {
					echo "<img src='" .$layer[0]->contentUrl ."' />";
				} else {
					?>
					<div id="flashHolder">
						<div style="position: absolute; z-index: 100; background-color: red">
							<span style="color: #ffffff" class="glyphicon glyphicon-move" aria-hidden="true"></span>
						</div>
						<div style="position: absolute; z-index: 0;">
							<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="<?php echo($layer[0]->width); ?>" height="<?php echo($layer[0]->height); ?>">
						        <param name="movie" value="<?php echo($layer[0]->contentUrl); ?>" />
						        <param name="scale" value="exactFit" />
						        <param name="wmode" value="transparent" />
						        <param name="allowscriptaccess" value="always" />
						        <!--[if !IE]>-->
						        <object type="application/x-shockwave-flash" data="<?php echo($layer[0]->contentUrl); ?>" width="<?php echo($layer[0]->width); ?>" height="<?php echo($layer[0]->height); ?>">
						        <!--<![endif]-->
						          <p>there should be a swf here</p>
						        <!--[if !IE]>-->
						        </object>
					        <!--<![endif]-->
							</object>
						</div>
					</div>
				<?php } ?>
			<?php //echo($layer[0]->contentUrl); ?><br />
			<?php //echo($layer[0]->contentType); ?>
		</div>
		</a>
	<?php } else { ?>
		<!-- <a style="cursor: default" href="<?php echo($layer[0]->clickthroughUrl); ?>" target="_blank"> -->
		<div id="layerPreviewHolder" class="draggable" style="display: block; width: 150px; height: 80px; background-color: red; left:<?php echo($layer[0]->xPos); ?>px; top:<?php echo($layer[0]->yPos); ?>px; position: absolute;">
			default layer placeholder
		</div>
	<!-- </a> -->
	<?php } ?>
</div>
<!-- upload modal div -->
<div class="modal fade bs-example-modal-sm layer-asset" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
                    <div class="modal-header">
                            <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                                    <span aria-hidden="true">×</span>
                            </button>
                            <h4 id="mySmallModalLabel" class="modal-title">Choose A Visual Asset</h4>
                    </div>
            <?php echo form_open_multipart('upload/do_layer_asset_upload/' .$layer[0]->id .'/'); ?>
                    <input type="file" name="asset_upload" size="20" />
                    <br /><br />
                    <input class="btn btn-primary" type="submit" value="upload">
                    <input type="hidden" name="return_to" value="Layers/preview/<?php echo $layer[0]->id; ?>/<?php echo $currentSwitchState; ?>" />
                    <input type="hidden" name="switch_num" value="<?php echo($switch[0]->id); ?>" />
            </form>
        </div>
      </div>
    </div>
    <!-- end of upload modal div -->
<script>
$("#layerPreviewHolder").draggable({cancel : 'object'});
$( "#layerPreviewHolder" ).on( "dragstop", function( event, ui ) {
	console.log('ON DRAG STOP::: ui = ', ui);
	console.log('x = ' +ui.position.left);
	console.log('y = ' +ui.position.top);
} );
$( "#layerPreviewHolder" ).on( "drag", function( event, ui ) {
	console.log('ON DRAG::: ui = ', ui);
	console.log('x = ' +ui.position.left);
	$('#layerX').val(Math.floor(ui.position.left));
	
	console.log('y = ' +ui.position.top);
	$('#layerY').val(Math.floor(ui.position.top));
} );
</script>