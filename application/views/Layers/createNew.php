<!-- IMAGE save success and error messages -->
<?php if (isset($_GET['upload']) && $_GET['upload'] == 'error') { ?>
        <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <?php echo $_GET['errors']; ?>
        </div>


<?php } else if (isset($_GET['upload']) && $_GET['upload'] == 'success') { ?>
        <div class="alert alert-success" role="alert">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                <span class="sr-only">Success:</span>
                Your upload completed
        </div>
<?php } ?>
<h1 style="padding-top: 50px;">create a new layer for <?php echo($group[0]->name); ?></h1>
<?php echo form_open('Layers/create'); ?>
	<div style="padding-left: 10px;" class="form-group">
    	<label for="layerTitle">Layer Title</label>
    	<input type="text" name="layerTitle" class="form-control" id="layerTitle" placeholder="Please Give This Layer a Title">
    </div>
    <div style="padding-left: 10px;" class="form-group">
    	<label for="switch">Switch</label>
		 <select name="switchId" class="form-control">
			 <?php foreach ($switches as $thisSwitch) { ?>
			 	<option value="<?php echo($thisSwitch->id); ?>"><?php echo($thisSwitch->title); ?></option>
			 <?php } ?>
		</select> 
    </div>
	<input type="hidden" name="group" value="<?php echo($group[0]->id); ?>">
	
	
	<div style="padding-left: 10px;"  class="form-group" style="padding-top: 10px;">
		<button type="submit" class="btn btn-default">Create</button>
	</div>
</form>