<h1 style="padding-top: 50px;">all the layers</h1>
<a class="btn btn-default" href="create/" role="button">New Layer</a><br /><br />
<?php 
	// echo "<pre>";
		// print_r($layers);
	// echo "</pre>";
?>
<?php foreach ($switch_codes as $thisEmbed) { ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo $thisEmbed->title; ?> <span style="color: grey;">(<?php echo($thisEmbed->lookup_hash); ?>)</span></h3>
		</div>
		<div class="panel-body">
			<?php foreach ($layers as $thisLayer) { 
				if ($thisLayer->switch_id == $thisEmbed->id) {
				?>
			<nav class="navbar navbar-default" style="padding-bottom: 60px; padding-top: 10px;">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <a class="navbar-brand" href="#">
			        <?php if ($thisLayer->contentType == 'image') { ?>
			        	<img alt="Brand" width="<?php echo($thisLayer->width); ?>" height="<?php echo($thisLayer->height); ?>" src="<?php echo($thisLayer->contentUrl); ?>">
			        <?php } else { ?>
			        		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="<?php echo($thisLayer->width); ?>" height="<?php echo($thisLayer->height); ?>">
						        <param name="movie" value="<?php echo($thisLayer->contentUrl); ?>" />
						        <param name="scale" value="exactFit" />
						        <param name="wmode" value="transparent" />
						        <param name="allowscriptaccess" value="always" />
						        <!--[if !IE]>-->
						        <object type="application/x-shockwave-flash" data="<?php echo($thisLayer->contentUrl); ?>" width="<?php echo($thisLayer->width); ?>" height="<?php echo($thisLayer->height); ?>">
						        <!--<![endif]-->
						          <p>there should be a swf here</p>
						        <!--[if !IE]>-->
						        </object>
					        <!--<![endif]-->
							</object>
			        <?php } ?>
			      </a>
			      
			      <ul class="nav navbar-nav navbar-right">
				  <li><a style="color: #000000; margin-right: 30px;" class="btn btn-primary" href="/liveSolutions/Layers/preview/<?php echo($thisLayer->id); ?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Preview and Set Position</a></li>
			      <li><a style="color: #000000;" class="btn <?php if ($thisLayer->live == 1) { echo "btn-success"; } else { echo "btn-default"; } ?>" href="/liveSolutions/Layers/liveShow/<?php echo($thisLayer->id); ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <?php if ($thisLayer->live == 1) { echo "currently visible"; } else { echo "show layer"; } ?></a></li>
			      <li><a style="color: #000000;" class="btn <?php if ($thisLayer->live == 0) { echo "btn-danger"; } else { echo "btn-default"; } ?>" href="/liveSolutions/Layers/liveHide/<?php echo($thisLayer->id); ?>"><span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span> <?php if ($thisLayer->live == 0) { echo "currently hidden"; } else { echo "hide layer"; } ?></a></li>
			      </ul>
			      
			    </div>
			  </div>
			</nav>
			<?php 
				}
			} ?>
		</div>
	</div>
	<div style="float: right;">
		<a style="margin-left: 10px;" class="btn btn-success" href="/liveSolutions/Layers/commitChanges/<?php echo($thisEmbed->id); ?>"><span class="glyphicon glyphicon-floppy-open" aria-hidden="true"></span> Commit these Changes</a>
	</div>
	<br />
	<br />
<?php } ?>