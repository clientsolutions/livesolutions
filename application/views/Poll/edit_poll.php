<?php
//var_dump($poll);
?>
<div style="margin-top: 80px;"></div>
<?php echo form_open_multipart('poll/update', array('role' => 'form')); 
  echo form_hidden('poll_id', $poll['question']->id);
?>
<?php
  echo form_input('title', $poll['question']->title, 'class="form-control"');
  echo "<br>";
  echo form_input('question', $poll['question']->question_text, 'class="form-control"');
?>
<br>
<div class="lead">Anwers</div>
<?php foreach ($poll['answer'] as $answer): ?>
  <?php //var_dump($answer); ?>
  <div class="form-group">
    <input class="form-control" name="answer_<?php echo $answer->id; ?>" value="<?php echo $answer->answer_text; ?>" />
  </div>
<?php endforeach; ?>

<?php echo form_submit('submit', 'Update', 'class="form-control btn btn-primary"'); ?>
<?php if (isset($updated) && $updated == true): ?>
  <div class="spacer"><br></div>
  <a href="/liveSolutions/poll/" class="btn btn-default form-control">Back</a>
<?php else: ?>
<?php endif; ?>