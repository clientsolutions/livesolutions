<?php
?>
<div style="margin-top: 80px;"></div>
<div class="container">
  <?php 
    echo form_open_multipart('poll/create', array('role' => 'form'));
  ?>
  <div class="form-group">
  <?php
    echo form_input('title', '', 'placeholder="Poll Title" class="form-control"');
  ?>
  </div>
  <div class="form-group">
  <?php
    echo form_input('question', '', 'placeholder="Question" class="form-control"');
  ?>
  </div><div class="form-group">
  <?php
    echo form_input('answer_1', '', 'placeholder="Answer 1" class="form-control"');
  ?>
  </div><div class="form-group">
  <?php
    echo form_input('answer_2', '', 'placeholder="Answer 2" class="form-control"');
  ?>
  </div><div class="form-group">
  <?php
    echo form_input('answer_3', '', 'placeholder="Answer 3" class="form-control"');
  ?>
  </div><div class="form-group">
  <?php
    echo form_input('answer_4', '', 'placeholder="Answer 4" class="form-control"');
  ?>
  </div><div class="form-group">
  <?php
    echo form_submit('submit', 'Create Poll', 'class="form-control btn btn-primary"');
  ?>
  </div>
</div>