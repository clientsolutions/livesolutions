<?php
?>
<div class="container">
  <a href="/liveSolutions/poll/create_form" class="btn btn-primary">Create Poll</a>
  <div style="height: 10px;"></div>
  <!-- Show a list of all polls -->
  <?php foreach ($polls as $poll): ?>
    <?php $pollTotal = 0; ?>
    <?php // var_dump($poll); ?>
    <div class="container" style="border: 1px solid;">
      <div class="pull-left">
      <a href="/liveSolutions/poll/edit/<?php echo $poll['question']->id; ?>" class="btn btn-default">Edit Poll</a>
      <a href="/liveSolutions/poll/delete/<?php echo $poll['question']->id; ?>" class="btn btn-danger">Delete Poll</a>
      <h4>Poll Title: <?php echo $poll['question']->title; ?></h4>
      <div>Poll Question: <?php echo $poll['question']->question_text; ?></div>
      <?php foreach ($poll['answer'] as $answer_key => $answer): ?>
        <?php $pollTotal += $answer->{'answer_count'}; ?>
        <?php // var_dump($answer->answer_text); ?>
        <div>Answer <?php echo ($answer_key + 1) . ': ' . $answer->{'answer_text'}; ?></div>
      <?php endforeach; ?>
      </div>
      <div class="pull-right">
        <div class="" style="">
          <?php foreach ($poll['answer'] as $key => $answer): ?>
          <a class="btn btn-default" href="/liveSolutions/poll/cast_vote_admin/<?php echo $answer->poll_id . '/' .$answer->id; ?>">
            Simulate Vote for <?php echo ($key + 1); ?>
          </a>
          <?php endforeach; ?>
        </div>
        <div style="height: 10px;"></div>
        <div>
          <!-- <p>Results</p> -->
          <p>Total Votes: <?php echo $pollTotal; ?></p>
          <?php foreach($poll['answer'] as $answer_key => $answer): ?>
            <p>Answer <?php echo $answer_key + 1; ?>:
            <?php echo $answer->{'answer_count'}; ?>
            <span style="margin-left: 10px;">
            	<?php 
            		if ($answer->answer_count > 0){
            			echo round((($answer->answer_count / $pollTotal) * 100 ), 1) . " %";
            		}
					else {
						echo "no responses";
					}
					 
            	?>
            </span>
            </p>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
</div>