<?php if(!defined('BASEPATH')) exit('no, no, no');
  
class Poll_model extends CI_Model {

  function __construct()
  {
    parent::__construct();
  }

  /**
  * Create poll
  */
  public function create_poll($data = array())
  {
    if (empty($data)) {
      return false;
    }

    // Unset the submit
    unset($data['submit']);

    // var_dump($data);

    $poll = array(
      'title' => $data['title'],
      'question_text' => $data['question']
    );
    // Create the poll to get the id
    $this->db->insert('poll', $poll);
    $poll_id = $this->db->insert_id();

    //Unset unneeded data
    unset($data['title']);
    unset($data['question']);

    // Loop through answers and insert
    foreach ($data as $answer) {      
      if (strlen($answer) > 0) {
        // var_dump($answer);
        // Insert answer
        $answer_array = array(
          'poll_id' => $poll_id,
          'answer_text' => $answer
        );
        $this->db->insert('poll_answer', $answer_array);
      }
    }
  }

  /**
  * Delete poll
  */
  public function delete($id = 0)
  {
    if ($id == 0) {
      return false;
    }

    $this->db->set('deleted', true);
    $this->db->where('id', $id);
    $this->db->update('poll');
    return true;
  }

  /**
  * Get all polls
  */
  public function get_all_polls()
  {
    $poll_array = array();
    $this->db->select('*');
    $this->db->where('deleted', false);
    $this->db->from('poll');
    $results = $this->db->get();
    if ($results->num_rows() > 0) {
      foreach ($results->result() as $key => $poll) {
        $pollObj['question'] = $poll;
        $pollObj['answer'] = array();
        $poll_answer = $this->db->get_where('poll_answer', array('poll_id' => $poll->id));
        if ($poll_answer->num_rows() > 0) {
          foreach ($poll_answer->result() as $key_2 => $answer) {
            array_push($pollObj['answer'], $answer);
          }
        }
        array_push($poll_array, $pollObj);
      }
    }

    return $poll_array;
  }

  /**
  * Get poll data for a single poll
  */
  public function get_poll_data($id = 0)
  {
    if ($id == 0) {
      return false;
    }

    $poll_query = $this->db->get_where('poll', array('id' => $id));

    if ($poll_query->num_rows() > 0) {
      $poll = $poll_query->result();
      $poll = $poll[0];
      $answer;
      $poll_answer = $this->db->get_where('poll_answer', array('poll_id' => $id));
      if ($poll_answer->num_rows() > 0) {
        $answer = $poll_answer->result();
        $poll_result['question'] = $poll;
        $poll_result['answer'] = $answer;
        return $poll_result;
      }
    }

    return false;
  }

  /**
  * poll update
  */
  public function update($form_data = array())
  {
    if (empty($form_data)) {
      return false;
    }

    $poll_data = array(
      'title' => $form_data['title'],
      'question_text' => $form_data['question']
    );

    // Update Poll
    $this->db->where('id', $form_data['poll_id']);
    $this->db->update('poll', $poll_data);

    // Prep answer
    unset($form_data['title']);
    unset($form_data['question']);
    unset($form_data['poll_id']);
    unset($form_data['submit']);

    // Cycle through remainder and update answers
    $answer_keys = array_keys($form_data);
    // var_dump($form_data);
    foreach ($answer_keys as $answer_key) {
      $answer_id = str_replace('answer_', '', $answer_key);
      // var_dump($answer_id);
      $answer_update = array(
        'answer_text' => $form_data['answer_'.$answer_id]
      );
      $this->db->where('id', $answer_id);
      $this->db->update('poll_answer', $answer_update);
    }

    return true;
  }

  /**
  * Cast vote (only increments by 1)
  */
  public function cast_vote($poll_id = 0, $answer_id = 0)
  {
    $this->db->set('answer_count', 'answer_count+1', FALSE);
    $this->db->where('poll_id', $poll_id);
    $this->db->where('id', $answer_id);
    $this->db->update('poll_answer');
  }

  /**
  * Get poll results
  */
  public function get_results($poll_id = 0)
  {
    if ($poll_id === 0) {
      return false;
    }


    $answers = array();
    $total = 0;

    $answer_query = $this->db->get_where('poll_answer', array('poll_id' => $poll_id));

    if($answer_query->num_rows() > 0) {
      // var_dump($answer_query->result());
      foreach ($answer_query->result() as $answer) {
        //var_dump($answer);
        $answers[] = array(
          'answer' => $answer->answer_text,
          'count' => $answer->answer_count
        );
        $total += $answer->answer_count;
      }
    }
    $answers['total'] = $total;

    return $answers;
  }

}