<?php
	if(!defined('BASEPATH')) exit('no, no, no');
	
	class EmbedSwitch_model extends CI_Model{
		
		function __construct() {
			parent::__construct();
		}
		
		public function fetchUsersEmbeds($userGroup) {
			$whereClause = 'group=' .$userGroup;
			$this->db->where($whereClause);
			
			$query = $this->db->get('switch_codes');
			return $query->result();
		}
		
		public function fetchAnEmbed($whichEmbed) {
			$whereClause = "id=" .$whichEmbed;
			$this->db->where($whereClause);
			
			$query = $this->db->get('switch_codes');
			return $query->result();
		}
		
		public function createNewSwitch($group, $switchTitle, $width, $height) {
			$data = array(
				'group' => $group,
				'title' => $switchTitle,
				'lookup_hash' => $this->generateEmbedHash(),
				'width'=>$width,
				'height'=>$height
			);
			
			$this->db->insert('switch_codes', $data);
			return $this->db->insert_id();
		}
		
		public function fetchStateCode($hash, $whatState) {
			$whereClause = "lookup_hash='" .$hash ."'";
			$this->db->where($whereClause);
			$this->db->select($whatState.'_state');
			
			$query = $this->db->get('switch_codes');
			$result = $query->result();
			
			
			switch ($whatState) {
				case 'pre':
					$stateCode = $result[0]->pre_state;
					break;
				case 'live':
					$stateCode = $result[0]->live_state;
					break;
				case 'post':
					$stateCode = $result[0]->post_state;
					break;
				case 'vod':
					$stateCode = $result[0]->vod_state;
					break;
				default:
					//return pre
					$stateCode = $result[0]->pre_state;
					break;
			}
			
			return $stateCode; 
		}
		
		////////////////////////////////////////////////
		//get switch details
		////////////////////////////////////////////////
		
		public function getSwitchByHash($switchHash) {
			$whereClause = 'lookup_hash="' .$switchHash .'"';
			$this->db->where($whereClause);
			
			$query = $this->db->get('switch_codes');
			return $query->result();
		}
		
		
		public function getSwitchById($switchId) {
			$whereClause = 'id="' .$switchId .'"';
			$this->db->where($whereClause);
			
			$query = $this->db->get('switch_codes');
			return $query->result();
		}
		
		public function getSwitchHash($whichSwitch) {
			$whereClause = 'id=' .$whichSwitch;
			$this->db->where($whereClause);
			$this->db->select('lookup_hash');
			
			$query = $this->db->get('switch_codes');
			$result = $query->result();
			
			return $result[0]->lookup_hash;
		}
		
		
		public function generateEmbedCode($switchId) {
			$whereClause = "id=" .$switchId;
			$this->db->where($whereClause);
			
			$query = $this->db->get('switch_codes');

			$returnedData = $query->result();
			$hash = $returnedData[0]->lookup_hash;
			$width = $returnedData[0]->width;
			$height = $returnedData[0]->height;
			//http://vpc-clientsolutions01.newrow.com/liveSolutions/embedSwitch/embed/$switchHash;
			$url = "http://csolcache.newrow.com/liveSolutions/embedSwitch/embed/" .$hash;
			
			//$url = "http://vpc-clientsolutions01.newrow.com/liveSolutions/embedSwitch/embed/" .$hash;
			
			$code = '<iframe scrolling="no" width="' .$width .'" height="' .$height .'" frameBorder="0" src="' .$url .'"></iframe>';
			return $code;
		}
		
		public function getSwitchCurrentState($switchId) {
			$whereClause = "id=" .$switchId;
			$this->db->where($whereClause);
			$this->db->select('current_state');
			$query = $this->db->get('switch_codes');

			$returnedData = $query->result();
			$currentState = $returnedData[0]->current_state;
			return $currentState;			
		}
		
		
		////////////////////////////////////////////////
		//saving pieces of data
		////////////////////////////////////////////////
		public function setSwitchCurrentState($switchId, $state) {
			$data['current_state'] = $state;
			$query = $this->db->update('switch_codes', $data, array('id'=>$switchId));
		}
		
		public function saveStateCode($switchNum, $state, $code, $type) {
			
			$data[$state.'_state'] = $code;
			$data[$state.'_type'] = $type;
			
			$query = $this->db->update('switch_codes', $data, array('id'=>$switchNum));
			//echo $this->db->last_query();
			
			//set the state type
		}
		
		////////////////////////////////////////////////
		//internal use tools
		////////////////////////////////////////////////
		public function generateEmbedHash() {
			$this->load->helper('string');
			return random_string('alnum', 16);
		}
		
		
		
		public function saveStateImgOrSwf($switchNum, $state, $objType, $pathToObject) {
			//save the image via cache
			//bust the cache (in case it's the same filename)
			//set the state type
		}
		
	}
?>