<?php
	if(!defined('BASEPATH')) exit('no, no, no');
	
	class Layers_model extends CI_Model{
		
		function __construct() {
			parent::__construct();
		}
		
		function createNewLayer($group, $title, $switchId) {
			$data = array(
				'group' => $group,
				'title' => $title,
				'switch_id' => $switchId,
				'preview' => 1
			);
			
			$this->db->insert('layers', $data);
			return $this->db->insert_id();
		}
		
		function fetchLayerById($layerId) {
			$whereClause = "id=" .$layerId;
			$this->db->where($whereClause);
			
			$query = $this->db->get('layers');
			return $query->result();
		}
		
		
		function fetchAllLayers($group) {
			$whereClause = "group=" .$group ." AND deleted=0";
			$this->db->where($whereClause);
			
			$query = $this->db->get('layers');
			return $query->result();
		}
		
		function getLayerInfoBySwitchId($switchId) {
			$whereClause = "switch_id=" .$switchId;
			$this->db->where($whereClause);
			
			$query = $this->db->get('layers');
			return $query->result();
		}
		
		
		//////////////////////////////////////////////////////////////////////
		///////saving data
		//////////////////////////////////////////////////////////////////////
		function showLayerLive($layerId) {
			$data['live'] = 1;
			$query = $this->db->update('layers', $data, array('id'=>$layerId));
		}
		
		function hideLayerLive($layerId) {
			$data['live'] = 0;
			$query = $this->db->update('layers', $data, array('id'=>$layerId));
		}
		
		
		function saveLayerPosition($x, $y, $layer) {
			$data['xPos'] = $x;
			$data['yPos'] = $y;
			$query = $this->db->update('layers', $data, array('id'=>$layer));
		}
		
		function saveLayerClickthrough($clickthrough, $layer) {
			$data['clickthroughUrl'] = $clickthrough;
			$query = $this->db->update('layers', $data, array('id'=>$layer));
		}
		
		function saveLayerAsset($layerId, $contentUrl, $contentType, $width, $height) {
			$data['contentUrl'] = $contentUrl;
			$data['contentType'] = $contentType;
			$data['width'] = $width;
			$data['height'] = $height;
			$query = $this->db->update('layers', $data, array('id'=>$layerId));
		}

		
	}
?>