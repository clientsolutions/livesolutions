<?php
defined('BASEPATH') OR exit('No Direct cript access allowed');

class EmbedSwitch extends CI_Controller{
	
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Embedswitch_model');
	}	
	
	public function index() {
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login');
		}
		
		//list all of the EmbedSwitches for the group of this user
		//title, code fetch, preview link, edit
		
		//this was hard coded
		//echo "user id = " .$this->session->userdata('user_id') .'<br />';
		
		$groupInfo = $this->ion_auth->get_users_groups($this->session->userdata('user_id'))->result();
		$group = $groupInfo[0]->id;
		// echo "<pre>";
			// print_r($group);
		// echo "</pre>";
		$embeds = $this->Embedswitch_model->fetchUsersEmbeds($group);
		
		$data = array('embeds' => $embeds);
		
		$this->load->view('header', $data);
		$this->load->view('EmbedSwitch/index', $data);
		$this->load->view('footer', $data);
		
	}
	
	public function code($whichSwitch) {
		
		$switchCode = $this->Embedswitch_model->generateEmbedCode($whichSwitch);
		$data = array('embedCode' => $switchCode);	
		
		$this->load->view('header', $data);
		$this->load->view('EmbedSwitch/code', $data);
		$this->load->view('footer', $data);
	}
	
	public function newSwitch() {
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login');
		}
		
		
		//echo "<pre>";
			//print_r($this->input->post());
		//echo "</pre>";
		
		if(($this->input->post('esTitle') && $this->input->post('group'))) {
			$group = $this->input->post('group');
			$switchTitle = $this->input->post('esTitle');
			
			$lastId = $this->Embedswitch_model->createNewSwitch($group, $switchTitle, $sWidth, $sHeight);
			
			//go to edit that
			redirect('EmbedSwitch/edit/'.$lastId); 
		}
		else {
			$group = $this->ion_auth->get_users_groups($this->session->userdata('user_id'))->result();
			$data['group'] = $group;
			
			$this->load->view('header', $data);
			$this->load->view('EmbedSwitch/new', $data);
			$this->load->view('footer', $data);			
		}
	}
	
	public function editSwitch($switchNum) {
		if($this->input->post('esTitle')) {
			$switchTitle = $this->input->post('esTitle');
			$width = $this->input->post('esWidth');
			$height = $this->input->post('esHeight');
			
			
			//go to edit that
			redirect('EmbedSwitch/edit/'.$lastId); 
		}
		else {
			$data = $this->Embedswitch_model->fetchAnEmbed($switchNum);
			
			$this->load->view('header', $data);
			$this->load->view('EmbedSwitch/edit', $data);
			$this->load->view('footer', $data);			
		}
	}
	
	public function embed($switchHash) {
		//actual code to be embedded
		$switchInfo = $this->Embedswitch_model->getSwitchByHash($switchHash);
		
		$data = array('switchInfo'=>$switchInfo);
		
				
		// echo "<pre>";
			// print_r($data);
		// echo "</pre>";
		
		
		//return the equivalent of main.php
		$this->load->view('EmbedSwitch/embed.php', $data);
		
	}
	
	////////////////////////////////////////////////////////////////
	//fetching the different states codes or objects individually
	////////////////////////////////////////////////////////////////
	
	public function state($state, $switchHash) {
		$stateContent = $this->Embedswitch_model->fetchStateCode($switchHash, $state);
		
		$data = array(
					'stateContent'=>$stateContent,
					'contentType'=>'ugh'
				);
		
		// echo "<pre>";
			// print_r($data);
		// echo "</pre>";
		
		
		//load the pre view
		//the view here should (based on the data)
		$this->output->set_header('Cache-Control: max-age=18000');
		$this->output->set_header('Expires:Thu, 19 Nov 2020 08:52:00 GMT');
		$this->output->set_header("I like spam");
		$this->output->set_header("Mike, Mike, Mike.");
		$this->output->set_header("Where is your soul?");
		$this->output->set_header("Do not laugh Paul- I know you are dead inside");
		$this->output->set_header("Pragma:");
		
		$this->load->view('EmbedSwitch/state', $data);
	}
	

	////////////////////////////////////////////////////////////////
	//set the state of the switch
	////////////////////////////////////////////////////////////////
	
	public function setSwitchState($switchState, $switchId) {
		$this->load->library('S3.php');
		
		//set the current state in the dB
		$this->Embedswitch_model->setSwitchCurrentState($switchId, $switchState);
		
		//create teh json 
		$jsonState = '{"state": "' .$switchState .'"}';
		
		//write the config file to amazon
		$switchHash = $this->Embedswitch_model->getSwitchHash($switchId);
		//$input =$this->s3->inputFile($jsonState);
		$uri = 'switchAssets/' .$switchHash .'/config.json';
		$bucket = 'clientsolutionsbucket';
		$metaHeaders = array();
		$headers = array("Content-Type"=>"application/json");
			
		if ($this->s3->putObject($jsonState, $bucket, $uri, 'public-read', $metaHeaders, $headers)) {
			//do nothing
			//echo "WORKED";
		} else {
			//echo "FAILED";
			redirect("/embedSwitch/viewSwitchState/$switchId?upload=error&errors=there was a failure to set the config, please try again");
		}
		
		redirect("/embedSwitch/viewSwitchState/$switchId?stateSet=success");
	}

	public function viewSwitchState($switchId) {
		
		$data = array();
		
		$switchData = $this->Embedswitch_model->getSwitchById($switchId);
		
		// echo "<br/><br/><br/><br/><br/><pre>";
			// print_r($switchData);
		// echo "</pre>";
		
		//get the current switch state
		$data['switchWidth'] = $switchData[0]->width;
		$data['switchHeight']= $switchData[0]->height;
		
		$data['currentSwitchState'] = $switchData[0]->current_state;
		$data['switchHash'] = $switchData[0]->lookup_hash;
		$data['switchId'] = $switchData[0]->id;
		
		$this->load->view('header', $data);
		$this->load->view('EmbedSwitch/viewState', $data);
		$this->load->view('footer', $data);
	}
	
	
	////////////////////////////////////////////////////////////////
	//editing the contents of the embed states
	////////////////////////////////////////////////////////////////
	
	public function saveData($switchNum, $state) {
		echo "switch number = " .$switchNum;
		echo "state = " .$state;
		print_r($_POST);
		
		$code = $this->input->post('code');
		$type = 'code';
		
		//save in the model
		$this->Embedswitch_model->saveStateCode($switchNum, $state, $code, $type);
		
		//get the hash of the embedCode 
		$hash = $this->Embedswitch_model->getSwitchHash($switchNum);
		
		//clear the cache
		$this->load->library('CloudfrontInvalidation');
		$this->cloudfrontinvalidation->invlidate("/liveSolutions/embedSwitch/state/$state/$hash");
		
		//send back to the return_to with a success message
		redirect($_REQUEST['return_to'] .'?codeSave=success');
		
	}
	
	public function edit($switchNum) {
		
		$embeds = $this->Embedswitch_model->fetchAnEmbed($switchNum);
		$data = array('embeds' => $embeds);
		
		// echo "<pre>";
			// print_r($data);
		// echo "</pre>";
		
		
		//save all of the states of a switch
		$this->load->view('header', $data);
		$this->load->view('EmbedSwitch/edit', $data);
		$this->load->view('footer', $data);
	}
	
	public function delete($switchNum) {
		//deletes the switch in question
	}
}
?>