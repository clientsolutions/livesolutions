<?php
defined('BASEPATH') OR exit('No Direct cript access allowed');

class Poll extends CI_Controller{

  function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login');
    }
    $this->load->model('poll_model', 'poll');
    $this->load->helper(array('form', 'url'));
  }

  public function index($prev_data = 0)
  {
    $data['polls'] = $this->poll->get_all_polls();
    $this->load->view('header', $data);
    $this->load->view('Poll/index', $data);
    $this->load->view('footer', $data);
  }

  /**
  * Edit the poll
  */
  public function edit($id = 0)
  {
    $poll = $this->poll->get_poll_data((int)$id);
    $data['poll'] = $poll;
    $this->load->view('header', $data);
    $this->load->view('Poll/edit_poll', $data);
    $this->load->view('footer', $data);
  }

  public function delete($id = 0)
  {
    $delete = $this->poll->delete($id);
    if($delete) {
      $data['deleted'] = true;
    }
    redirect('poll/index');
  }

  /**
  * Create poll
  */
  public function create()
  {
    $form_data = $this->input->post();

    $create = $this->poll->create_poll($form_data);
    redirect('poll/index');
  }

  /**
  * Output poll creation form
  */
  public function create_form()
  {
    $this->load->view('header');
    $this->load->view('Poll/create_form');
    $this->load->view('footer');
  }

  public function update()
  {
    $form_data = $this->input->post();
    // var_dump($form_data);
    $update = $this->poll->update($form_data);

    // Back to update with updated data
    $poll = $this->poll->get_poll_data((int)$form_data['poll_id']);
    $data['poll'] = $poll;
    $data['updated'] = $poll;
    $this->load->view('header', $data);
    $this->load->view('Poll/edit_poll', $data);
    $this->load->view('footer', $data);

  }

  public function cast_vote($poll_id = 0, $answer_id = 0)
  {
    $casted = $this->poll->cast_vote($poll_id, $answer_id);
  }

  public function cast_vote_admin($poll_id = 0, $answer_id = 0)
  {
    $casted = $this->poll->cast_vote($poll_id, $answer_id);
    redirect('poll/index');
  }

  public function get_results($poll_id = 0)
  {
    $results = $this->poll->get_results($poll_id);
    if ($results) {
      echo json_encode(array('status' => 'success', 'data' => $results));
      return;
    }
    echo json_encode(array('status' => 'error'));
  }

  public function get_poll($id = 0)
  {
    $poll = $this->poll->get_poll_data($id);
    if ($poll) {
      echo json_encode(array('status' => 'success', 'data' => $poll));
      return;
    }
    echo json_encode(array('status' => 'error'));
  }

}
