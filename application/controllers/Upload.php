<?php

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	function index()
	{
		$this->load->view('upload_form', array('error' => ' ' ));
	}

	function do_upload($switchNum) {
		$field_name = "pre_upload";
		//$data['return_to']=$_POST['return_to'];
		
		//print_r($_REQUEST);

		$config['upload_path'] = './switchAssets/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($field_name))
		{
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
			
			redirect($_REQUEST['return_to'] .'?upload=error&errors='.implode(",", $error));
			//$this->load->view('upload_form', $error);
		}
		else
		{
				
			$this->load->library('S3.php');
			$this->load->model('Embedswitch_model');
			
			$data = array('upload_data' => $this->upload->data());
			$state = $this->input->post('state');

			///////////////////////////////////////////////////
			//handle an amazon put
			///////////////////////////////////////////////////
			
			$switchHash = $this->Embedswitch_model->getSwitchHash($switchNum);
			$input =$this->s3->inputFile($this->upload->data('full_path'));
			$uri = 'switchAssets/' .$switchHash .'/' .$this->upload->data('file_name');
			$bucket = 'clientsolutionsbucket';
			
			if ($this->s3->putObject($input, $bucket, $uri, 'public-read')) {
				//do nothing
				//clear the cache
				$this->load->library('CloudfrontInvalidation');
				$this->cloudfrontinvalidation->invlidate("/liveSolutions/embedSwitch/state/$state/$switchHash");
				
			} else {
				redirect($_REQUEST['return_to'] .'?upload=error&errors=there was a failure to upload your asset, please try again');
			}
			
			//actual url
			//http://clientsolutionsbucket.s3.amazonaws.com/switchAssets/[switch_code]/[filename]
			
			
			//if this was an image, wrap it in an image tag
			if ($data['upload_data']['is_image'] == 1) {
				$codeToInsert = '<img src="';
				$codeToInsert .= 'http://clientsolutionsbucket.s3.amazonaws.com/switchAssets/' .$switchHash .'/' .$data['upload_data']['file_name'];
				$codeToInsert .='">';
				
				//echo "<br />$codeToInsert";
				$type = 'image';
			}
			
			
			$this->Embedswitch_model->saveStateCode($switchNum, $state, $codeToInsert, $type);
			
			//if this was a swf, do the swf thing
			//echo "<pre>";
			//print_r($data);
			//echo "</pre>";
			redirect($_REQUEST['return_to'] .'?upload=success');
			//$this->load->view('upload_success', $data);
		}
	}

function do_layer_asset_upload($layerId) {
		$field_name = "asset_upload";
		//$data['return_to']=$_POST['return_to'];
		
		//print_r($_REQUEST);

		$config['upload_path'] = './switchAssets/';
		$config['allowed_types'] = 'gif|jpg|png|swf';
		$config['max_size']	= '1000000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($field_name))
		{
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
			
			redirect($_REQUEST['return_to'] .'?upload=error&errors='.implode(",", $error));
			//$this->load->view('upload_form', $error);
		}
		else
		{
			
			// echo "<pre>";
				// print_r($this->upload->data());
			// echo "</pre>";
			// die();
			// die();
// 			
			$this->load->library('S3.php');
			$this->load->model('Embedswitch_model');
			$this->load->model('Layers_model');
			
			$data = array('upload_data' => $this->upload->data());
			$state = $this->input->post('state');
			$switchNum = $this->input->post('switch_num');

			///////////////////////////////////////////////////
			//handle an amazon put
			///////////////////////////////////////////////////
			
			$switchHash = $this->Embedswitch_model->getSwitchHash($switchNum);
			$input =$this->s3->inputFile($this->upload->data('full_path'));
			$uri = 'switchAssets/' .$switchHash .'/' .$this->upload->data('file_name');
			$bucket = 'clientsolutionsbucket';
			
			if ($this->s3->putObject($input, $bucket, $uri, 'public-read')) {
				//do nothing
				//clear the cache
				//$this->load->library('CloudfrontInvalidation');
				//$this->cloudfrontinvalidation->invlidate("/liveSolutions/embedSwitch/state/$state/$switchHash");
				
			} else {
				redirect($_REQUEST['return_to'] .'?upload=error&errors=there was a failure to upload your asset, please try again');
			}
			
			
			
			//actual url
			//http://clientsolutionsbucket.s3.amazonaws.com/switchAssets/[switch_code]/[filename]
			
			
			//if this was an image, wrap it in an image tag
			if ($data['upload_data']['is_image'] == 1) {
				
				$contentUrl = 'http://clientsolutionsbucket.s3.amazonaws.com/switchAssets/' .$switchHash .'/' .$data['upload_data']['file_name'];
				$contentType = 'image';
			}
			else {
				//this better be an effin swf.
				$contentUrl = 'http://clientsolutionsbucket.s3.amazonaws.com/switchAssets/' .$switchHash .'/' .$data['upload_data']['file_name'];
				$contentType = 'swf';
			}
			echo "IMAGE SIZE<br />";
			$imageInfo = getimagesize($contentUrl);
			//print_r(getimagesize($contentUrl));
			$width = $imageInfo[0];
			$height = $imageInfo[1];
			// die();
			// die();
			
			$this->Layers_model->saveLayerAsset($layerId, $contentUrl, $contentType, $width, $height);
			//$this->Embedswitch_model->saveStateCode($switchNum, $state, $codeToInsert, $type);
			
			//if this was a swf, do the swf thing
			//echo "<pre>";
			//print_r($data);
			//echo "</pre>";
			redirect($_REQUEST['return_to'] .'?upload=success');
			//$this->load->view('upload_success', $data);
		}
	}
}
?>