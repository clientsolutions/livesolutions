<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Layers extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
      		redirect('auth/login');
    	}
		//load the model for all the functions
		$this->load->helper(array('form', 'url'));
		$this->load->model('Layers_model');
	}
	
	function index() {
		echo'controller index for layers';
		
		$groups = $this->ion_auth->get_users_groups($this->session->userdata('user_id'))->result();
		$group = $groups[0]->id;
		
		//get all of the switches for this group
		$this->load->model('Embedswitch_model');		
		$data['switch_codes'] = $this->Embedswitch_model->fetchUsersEmbeds($group);
		
		//get all of the layers
		$data['layers'] = $this->Layers_model->fetchAllLayers($group);
		 
		
		$this->load->view('header', $data);
		$this->load->view('Layers/index', $data);
		$this->load->view('footer', $data);
	}
	
	function create() {
		
		if(($this->input->post('layerTitle') && $this->input->post('group') && $this->input->post('switchId'))) {
			//we have a submissions
			$group = $this->input->post('group');
			$layerTitle = $this->input->post('layerTitle');
			$switchId = $this->input->post('switchId');
			
			$lastId = $this->Layers_model->createNewLayer($group, $layerTitle, $switchId);
			
			//go to edit that
			redirect('Layers/preview/'.$lastId); 
		}
		
		$this->load->model('Embedswitch_model');
		
		$group = $this->ion_auth->get_users_groups($this->session->userdata('user_id'))->result();
		$data['group'] = $group;
		
		$switches = $this->Embedswitch_model->fetchUsersEmbeds($group[0]->id);
		$data['switches'] = $switches;
		
		$this->load->view('header', $data);
		$this->load->view('Layers/createNew', $data);
		$this->load->view('footer', $data);
	}
	
	function delete($layerId) {
		//mark it as deleted and don't show anymore
	}
	
	
	function preview($layerId, $switchState='live') {
		//list the layers in a panel to be set for the switch state
		//
		$data['layer'] = $this->Layers_model->fetchLayerById($layerId);
		
		$this->load->model('Embedswitch_model');
		$data['switch'] = $this->Embedswitch_model->fetchAnEmbed($data['layer'][0]->switch_id);
		
		$data['currentSwitchState'] = $switchState;
		
		$this->load->view('header', $data);
		$this->load->view('Layers/preview', $data);
		$this->load->view('footer', $data);
		
	}
	
	
	////////////////////////////////////////////////////////
	//api type functions for the preview state to call
	////////////////////////////////////////////////////////
	function preveiwShow($whichLayer) {
		//this should be a call to set the specified layer to visible for the preview
	}
	
	function preveiwHide($whichLayer) {
		//this should be a call to set the specified layer to visible for the preview
	}
	
	function liveShow($layerId) {
		//make the layer live in th db
		$this->Layers_model->showLayerLive($layerId);
		
		//send the user back		
		redirect('Layers/');
	}
	
	function liveHide($layerId) {
		//make the layer hidden for live in th db
		
		$this->Layers_model->hideLayerLive($layerId);
		
		//send the user back		
		redirect('Layers/');
	}
	
	function commitChanges($switchId) {
		
		//get all of the layers that are affiliated with this switch with their urls and states
		$switchLayers = $this->Layers_model->getLayerInfoBySwitchId($switchId);
		
		
		//write those in a json file to amazon under the filename of layersConfig.json
		$this->load->model('Embedswitch_model');
		$this->load->library('S3.php');
		
		// echo "<pre>";
			// print_r($switchLayers);
		// echo "</pre>";
		
		$switchHash = $this->Embedswitch_model->getSwitchHash($switchId);
		$jsonLayersConfig = json_encode($switchLayers);
		 
		// echo "<pre>";
			// print_r($switchHash);
		// echo "</pre>";
// 		
		// echo "<pre>";
			// print_r($jsonLayersConfig);
		// echo "</pre>";
		// die();
		// die();
		
		//$input =$this->s3->inputFile($jsonState);
		$uri = 'switchAssets/' .$switchHash .'/layerConfig.json';
		$bucket = 'clientsolutionsbucket';
		$metaHeaders = array();
		$headers = array("Content-Type"=>"application/json");
			
		if ($this->s3->putObject($jsonLayersConfig, $bucket, $uri, 'public-read', $metaHeaders, $headers)) {
			//do nothing
			//echo "WORKED";
		} else {
			//echo "FAILED";
			redirect("/Layers/?upload=error&errors=there was a failure to set the layer config, please try again");
		}
		
		redirect("/Layers/?stateSet=success");
		
	}
	
	function saveUrl() {
		if($this->input->post('layerId')) {
			//we have a submissions
			$clickthroughUrl = $this->input->post('clickthroughUrl');
			$layerId = $this->input->post('layerId');
			$currentState = $this->input->post('previewState');
			
			$this->Layers_model->saveLayerClickthrough($clickthroughUrl, $layerId);
			
			//go to edit that
			redirect('Layers/preview/'.$layerId .'/'.$currentState); 
		}
		else {
			echo "in the else statement";			
		}
	}
	
	function savePosition() {
		//save the x/y position of the layer
		if(($this->input->post('layerX') && $this->input->post('layerY'))) {
			//we have a submissions
			$layerX = $this->input->post('layerX');
			$layerY = $this->input->post('layerY');
			$layerId = $this->input->post('layerId');
			$currentState = $this->input->post('previewState');
			
			$this->Layers_model->saveLayerPosition($layerX, $layerY, $layerId);
			
			//go to edit that
			redirect('Layers/preview/'.$layerId .'/'.$currentState); 
		}
	}
	
}